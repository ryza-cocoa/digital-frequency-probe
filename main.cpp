//
//  main.cpp
//  Digital Frequency Probe
//
//  Created by Ryza on 15/8/12.
//  Copyright © 2015 Ryza. All rights reserved.
//

//#define RASPBERRY

#ifndef RASPBERRY
#define INT_EDGE_FALLING 0
#define HIGH 1
#define LOW  0
#define OUTPUT 1
int wiringPiSetup();
int wiringPiISR(int, int, void (*func)(void));
int delay(int);
void delayMicroseconds(int);
void digitalWrite(int, int);
void pinMode(int,int);
#else
#include <wiringPi.h>
#endif

#include <chrono>
#include <csignal>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <mutex>
#include <sys/time.h>
#include <thread>

/* +-----+----------+-----------+--------------+---Raspberry Pi 2 Model B---+-----------------------+-----------+----------+-----+  */
/* | BCM | wiringPi |    Name   |  Attach to   |        Physical PIN        |       Attach to       |   Name    | wiringPi | BCM |  */
/* +-----+----------+-----------+--------------+-------------++-------------+-----------------------+-----------+----------+-----+  */
/* |     |          |     3.3v  | LCD(A)       |          1  ||  2          | LM358P(VCC), LCD(VDD) |  5v       |          |     |  */
/* |   2 |      8   |    SDA.1  |              |          3  ||  4          | TTL->USB(5V)          |  5v       |          |     |  */
/* |   3 |      9   |    SCL.1  |              |          5  ||  6          | TTL->USB(GND)         |  0v       |          |     |  */
/* |   4 |      7   | GPIO # 7  | LCD(RS)      |          7  ||  8          | TTL->USB(RXD)         |  TxD      |   15     | 14  |  */
/* |     |          |       0v  |              |          9  ||  10         | TTL->USB(TXD)         |  RxD      |   16     | 15  |  */
/* |  17 |      0   | GPIO #00  |              |         11  ||  12         | Frequency Probe       |  GPIO #01 |   1      | 18  |  */
/* |  27 |      2   | GPIO #02  |              |         13  ||  14         | LCD(D5)               |  0v       |          |     |  */
/* |  22 |      3   | GPIO #03  |              |         15  ||  16         | LCD(D6)               |  GPIO #04 |   4      | 23  |  */
/* |     |          |     3.3v  |              |         17  ||  18         |                       |  GPIO #05 |   5      | 24  |  */
/* |  10 |     12   |     MOSI  |              |         19  ||  20         |                       |  0v       |          |     |  */
/* |   9 |     13   |     MISO  |              |         21  ||  22         |                       |  GPIO #06 |   6      | 25  |  */
/* |  11 |     14   |     SCLK  |              |         23  ||  24         |                       |  CE0      |   10     | 8   |  */
/* |     |          |       0v  |              |         25  ||  26         |                       |  CE1      |   11     | 7   |  */
/* |   0 |     30   |    SDA.0  |              |         27  ||  28         |                       |  SCL.0    |   31     | 1   |  */
/* |   5 |     21   | GPIO #21  |              |         29  ||  30         | LCD(K)                |  0v       |          |     |  */
/* |   6 |     22   | GPIO #22  |              |         31  ||  32         | LCD(D7)               |  GPIO #26 |   26     | 12  |  */
/* |  13 |     23   | GPIO #23  | LCD(E)       |         33  ||  34         | LM358P(GND), LCD(VSS) |  0v       |          |     |  */
/* |  19 |     24   | GPIO #24  | LCD(D4)      |         35  ||  36         |                       |  GPIO #27 |   27     | 16  |  */
/* |  26 |     25   | GPIO #25  |              |         37  ||  38         |                       |  GPIO #28 |   28     | 20  |  */
/* |     |          |       0v  | LCD(RW)      |         39  ||  40         |                       |  GPIO #29 |   29     | 21  |  */
/* +-----+----------+-----------+--------------+-------------++-------------+-----------------------+-----------+----------+-----+  */
/* | BCM | wiringPi | Name      |  Attach to   |        Physical PIN        |       Attach to       | Name      | wiringPi | BCM |  */
/* +-----+----------+-----------+--------------+---Raspberry Pi 2 Model B---+-----------------------+-----------+----------+-----+  */

#pragma mark
#pragma mark - Declaration

/**
 *  @brief  数字频率探针引脚
 */
#define DIGITAL_FREQ_PROBE_PIN 1

/**
 *  @brief  LCD1602 RS引脚
 */
#define LCD1602_RS 7

/**
 *  @brief  LCD1602 E引脚
 */
#define LCD1602_EN 23

/**
 *  @brief  LCD1602 数据引脚
 */
const int DB[] = {24,4,5,26};

/**
 *  @brief  LCD1602 第一行起始
 */
#define DISPLAY_LINE_1 0x80

/**
 *  @brief  LCD1602 第二行起始
 */
#define DISPLAY_LINE_2 0xC0

/**
 *  @brief  全局计数器
 */
static volatile unsigned long long global_counter = 0;

/**
 *  @brief  刷新函数的锁
 */
std::mutex refresh_mutex;

/**
 *  @brief  全局开始时间
 */
static auto start = std::chrono::high_resolution_clock::now();

/**
 *  @brief  单次开始时间
 */
static auto substart = std::chrono::high_resolution_clock::now();

/**
 *  @brief  单次结束时间
 */
static auto subend = std::chrono::high_resolution_clock::now();

/**
 *  @brief 初始化数字频率探针
 */
bool initFreqProbe();

/**
 *  @brief 初始化LCD
 */
void initLCD(void);

/**
 *  @brief  初始化Timer信号操作
 */
void init_sigaction(void);

/**
 *  @brief  初始化Timer
 */
void init_timer(void);

/**
 *  @brief  向LCD中写入数据
 *
 *  @param bits 数据
 *  @param mode LCD E段电平高低
 */
void lcd_write_byte(int bits, bool mode);

/**
 *  @brief  在LCD中显示字符串
 *
 *  @param msg 将要现实的字符串
 *
 *  @discussion 若超过16个字符, 则自动截取
 */
void lcd_print(const char * msg);

/**
 *  @brief  处理SIGINT信号
 */
void signal_handler(int signal);

/**
 *  @brief  Timer信号触发后执行的函数
 */
void refresh(int signal);

/**
 *  @brief  数字频率探针下降沿中断
 */
void gpio_interrupt(void);

#pragma mark
#pragma mark - Implementation

/**
 *  @brief 初始化数字频率探针
 */
bool initFreqProbe() {
    if (wiringPiSetup() < 0) {
        fprintf (stderr, "Unable to setup wiringPi: %s\n", strerror(errno));
        return false;
    }
    if (wiringPiISR(DIGITAL_FREQ_PROBE_PIN, INT_EDGE_FALLING, &gpio_interrupt) < 0) {
        fprintf (stderr, "Unable to setup ISR: %s\n", strerror(errno)) ;
        return false;
    }
    return true;
}

/**
 *  @brief 初始化LCD
 */
void initLCD(void) {
    pinMode(LCD1602_RS,OUTPUT);
    pinMode(LCD1602_EN,OUTPUT);
    pinMode(DB[0],OUTPUT);
    pinMode(DB[1],OUTPUT);
    pinMode(DB[2],OUTPUT);
    pinMode(DB[3],OUTPUT);
    lcd_write_byte(0x28, false);
    delayMicroseconds(50);
    lcd_write_byte(0x06, false);
    delayMicroseconds(50);
    lcd_write_byte(0x0c, false);
    delayMicroseconds(50);
    lcd_write_byte(0x80, false);
    delayMicroseconds(50);
    lcd_write_byte(0x01, false);
}

/**
 *  @brief  初始化Timer信号操作
 */
void init_sigaction(void) {
    struct sigaction act;
    act.sa_handler = refresh;
    act.sa_flags = 0;
    sigemptyset(&act.sa_mask);
    sigaction(SIGPROF, &act, NULL);
}

/**
 *  @brief  初始化Timer
 */
void init_timer(void) {
    struct itimerval val;
    val.it_value.tv_sec = 0;
    val.it_value.tv_usec = 864000;
    val.it_interval = val.it_value;
    setitimer(ITIMER_PROF, &val, NULL);
}

/**
 *  @brief  向LCD中写入数据
 *
 *  @param bits 数据
 *  @param mode LCD E段电平高低
 */
void lcd_write_byte(int bits, bool mode) {
    digitalWrite( LCD1602_RS, mode ? HIGH : LOW);
    digitalWrite( DB[0], LOW);
    digitalWrite( DB[1], LOW);
    digitalWrite( DB[2], LOW);
    digitalWrite( DB[3], LOW);
    
    if ((bits&0x10)==0x10) digitalWrite( DB[0], HIGH);
    if ((bits&0x20)==0x20) digitalWrite( DB[1], HIGH);
    if ((bits&0x40)==0x40) digitalWrite( DB[2], HIGH);
    if ((bits&0x80)==0x80) digitalWrite( DB[3], HIGH);
    delayMicroseconds(50);
    digitalWrite( LCD1602_EN, HIGH);
    delayMicroseconds(50);
    digitalWrite( LCD1602_EN, LOW);
    delayMicroseconds(50);
    digitalWrite( DB[0], LOW);
    digitalWrite( DB[1], LOW);
    digitalWrite( DB[2], LOW);
    digitalWrite( DB[3], LOW);
    
    if ((bits&0x01)==0x01) digitalWrite( DB[0], HIGH);
    if ((bits&0x02)==0x02) digitalWrite( DB[1], HIGH);
    if ((bits&0x04)==0x04) digitalWrite( DB[2], HIGH);
    if ((bits&0x08)==0x08) digitalWrite( DB[3], HIGH);
    delayMicroseconds(50);
    digitalWrite( LCD1602_EN, HIGH);
    delayMicroseconds(50);
    digitalWrite( LCD1602_EN, LOW);
    delayMicroseconds(50);
}

/**
 *  @brief  在LCD中显示字符串
 *
 *  @param msg 将要现实的字符串
 *
 *  @discussion 若超过16个字符, 则自动截取
 */
void lcd_print(const char * msg) {
    size_t length = strlen(msg);
    if (length < 16) {
        for (size_t i = 0; i < length; i++) {
            lcd_write_byte(msg[i], true);
        }
        for (size_t i = length; i < 16; i++) {
            lcd_write_byte(' ', true);
        }
    } else {
        for (int i = 0; i < 16; i++) {
            lcd_write_byte(msg[i], true);
        }
    }
}

/**
 *  @brief  处理SIGINT信号
 */
void signal_handler(int signal) {
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> diff = end-start;
    std::cout << "\nTime: " << diff.count() << " s\n";
    std::cout << "Counts: " << global_counter << "\n";
    std::cout << "Freq: " << global_counter/diff.count() << " Hz\n";
    exit(0);
}

/**
 *  @brief  数字频率探针下降沿中断
 */
void gpio_interrupt(void) {
    ++global_counter;
}

/**
 *  @brief  Timer信号触发后执行的函数
 */
void refresh(int signal) {
    refresh_mutex.lock();
    static long long last = 0;
    subend = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> diff = subend - substart;
    double freq = (global_counter - last)/diff.count();
    std::cout << "Freq: " << freq << " Hz\n";
    char buffer[16] = {0};
    if (freq > 1000000) {
        freq /= 1000000;
        sprintf(buffer, "Freq: %.6lfMHz", freq);
    } else if (freq > 1000) {
        freq /= 1000;
        sprintf(buffer, "Freq: %7.2lfkHz", freq);
    } else {
        sprintf(buffer, "Freq: %8.2lfHz", freq);
    }
    lcd_write_byte(DISPLAY_LINE_1, false);
    lcd_print(buffer);
    last = global_counter;
    substart = std::chrono::high_resolution_clock::now();
    refresh_mutex.unlock();
}

int main() {
    if (initFreqProbe()) {
        initLCD();
        init_sigaction();
        init_timer();
        signal(SIGINT, signal_handler);
        start = std::chrono::high_resolution_clock::now();
        substart = std::chrono::high_resolution_clock::now();
        global_counter = 0;
        for (;;) {}
    }
    return 0;
}
